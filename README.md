# Forge Jira Issue Unfurls for Slack

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE)

This is an example [Forge](https://developer.atlassian.com/platform/forge/) app that [unfurls](https://api.slack.com/docs/message-link-unfurling) Jira issue links shared in Slack messages. It is designed to demonstrate [web triggers](https://developer.atlassian.com/platform/forge/web-triggers/), a Forge feature for consuming HTTP requests from external sources (like Slack!)

## Usage

Whenever a Slack message contains one or more links to a Jira issue, this app attaches previews of those issues to the Slack message.

![Screenshot of Slack messages](./screenshot.png)

This app is designed as a Forge reference example for developers. There are no user-facing configuration options, but you can modify the content and format of the issue preview by editing the `formatIssueAttachment()` function in `slack-async-webtrigger.js`.

## Architecture

This app uses two webtriggers to handle a Slack event subscription. The `slack-webtrigger.js` accepts and responds to Slack event webhooks, but forwards the event payload to a separate web trigger (`slack-async-webtrigger.js`) to handle interacting with Jira and posting the message preview back to Slack.

```
+-------+          +------------+   +------------------+     +-------+
| Slack |          | Webtrigger |   | Async Webtrigger |     | Jira  |
+-------+          +------------+   +------------------+     +-------+
    |                     |                     |                |
    | link_shared event   |                     |                |
    |-------------------->|                     |                |
    |                     |                     |                |
    |                     | link_shared event   |                |
    |                     |-------------------->|                |
    |                     |                     |                |
    |              200 OK |                     |                |
    |<--------------------|                     |                |
    |                                           |                |
    |                                           | Fetch issue    |
    |                                           |--------------->|
    |                                           |
    |                               chat.unfurl |
    |<------------------------------------------|

```

This indirection is needed as Slack event subscription webhooks requests [must be responded to within 3000ms](https://api.slack.com/events-api#responding_to_events), or Slack considers the event delivery a failure and attempts to retry (which would result in multiple message events being dispatched to your webtrigger). Fetching the issue from Jira Cloud and rendering the issue can occasionally take longer than 3000ms, so the second webtrigger allows our app to always respond to Slack in a timely manner.

Invoking one webtrigger from another webtrigger is a bit of a hack. Eventually, Forge will support a native mechanism to handle asynchronous jobs, but this works in the meantime :)

## Installation

**Note:** You will need permission to create and install a Slack app into your team's Slack workspace. Alternatively, create a new Slack workspace for testing purposes at [slack.com](https://slack.com).

1. Follow the instructions on [Example Apps](https://developer.atlassian.com/platform/forge/example-apps/) to copy, deploy, and install this Forge app.
1. Create a new Slack app at [api.slack.com](https://api.slack.com) (you can name it _Jira Issue Unfurls_, or whatever you like).
1. In the Slack app's configuration, click on **OAuth & Permissions**.
1. Scroll down to **Bot Token Scopes** and add the `links:read` and `links:write` scopes.
1. Click **Install app into Workspace** at the top of the page and install the app into your workspace.
1. Set an encrypted [environment variable](https://developer.atlassian.com/platform/forge/environments/) keyed by `SLACK_OAUTH_BEARER_TOKEN` with a value of the **Bot User OAuth Access Token**. `forge variables:set --encrypt SLACK_OAUTH_BEARER_TOKEN xxxxxxxxxx`.
1. In the Slack app's configuration, click on **Event Subscriptions**.
1. Toggle **Enable events** to 'On'.
1. From the CLI, run `forge install:list`, and copy the **Installation ID** corresponding to your Jira site
1. Run `forge webtrigger` and paste in the **Installation ID** you just copied.
1. Select the `async-webtrigger` web trigger.
1. _Carefully_ copy the webtrigger URL and set it as the value of the `ASYNC_WEBTRIGGER_URL` environment variable. There is no need to encrypt this variable. `forge variables:set ASYNC_WEBTRIGGER_URL your_webtrigger_url`.
1. Run `forge deploy` to deploy the changes to your environment variables.
1. Run `forge webtrigger` again, and paste in the **Installation ID** copied from a few steps back. 
1. Select the `slack-webtrigger` web trigger this time.
1. Carefully copy the webtrigger URL and paste it into the **Request URL** field on the **Event Subcriptions** page of your Slack app configuration. Press `tab` to unfocus on the input field (this will cause Slack to verify the URL).
1. Expand the **App unfurl domains** section at the bottom of the page. 
1. Click **Add Domain**.
1. Add your Jira site's URL as a domain (e.g. `teamsinspace.atlassian.net`).
1. Click **Save Changes**.
1. If prompted, click **reinstall your app** to update the permission scopes for your Slack app.
1. You're done! Test out the app by sending a Slack message containing a link to a Jira issue (e.g. `https://example.atlassian.net/browse/ABC-123`).

## Debugging

You can enable verbose logging by setting the `DEBUG_LOGGING` [environment variable](https://developer.atlassian.com/platform/forge/environments/) to `1`. Logs can then be viewed with the `forge logs` command.

Alternatively, you can use the [`forge tunnel`](https://developer.atlassian.com/platform/forge/change-the-frontend-with-forge-ui/#set-up-tunneling) command to run your Forge app locally. Note that you must pass the environment variable values to the tunnel with the prefix `FORGE_USER_VAR_`, e.g.:

```
FORGE_USER_VAR_SLACK_OAUTH_BEARER_TOKEN=your_secret_bot_token_here FORGE_USER_VAR_ASYNC_WEBTRIGGER_URL=your_async_webtrigger_url_here FORGE_USER_VAR_DEBUG_LOGGING=1 forge tunnel
```

## License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

[![With â¤ï¸ from Atlassian](https://raw.githubusercontent.com/atlassian-internal/oss-assets/master/banner-cheers.png)](https://www.atlassian.com)
